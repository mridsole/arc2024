from typing import *
from numpy.typing import *
from dataclasses import dataclass
from enum import Enum
from arc.puzzle import Grid, Sample, Puzzle
import numpy as np


@dataclass
class FlipAugment:
    axis: int

    @classmethod
    def sample(cls):
        return cls(axis=np.random.randint(0,1))


@dataclass
class RotateAugment:
    rot: int

    @classmethod
    def sample(cls):
        return cls(rot=np.random.randint(0,3))


@dataclass
class PermuteColorAugment:
    permutation: NDArray[np.int8]

    @classmethod
    def sample(cls):
        return cls(permutation=np.random.permutation(np.arange(0, 10, dtype=np.int8)))


def random_primitive_augment():
    return (FlipAugment, RotateAugment, PermuteColorAugment)[np.random.randint(0,2)].sample()


@dataclass
class SequenceAugment:
    sequence: List['Augment']

    @classmethod
    def sample(cls):
        N = np.random.randint(1,4)
        return cls(sequence=[ random_primitive_augment() for _ in range(N) ])


Augment =  FlipAugment | RotateAugment | PermuteColorAugment | SequenceAugment


def augment_grid(x: Grid, aug: Augment) -> Grid:
    if isinstance(aug, FlipAugment):
        return np.flip(x, aug.axis)
    elif isinstance(aug, RotateAugment):
        return np.rot90(x, aug.rot)
    elif isinstance(aug, PermuteColorAugment):
        return aug.permutation[x]
    elif isinstance(aug, SequenceAugment):
        for aug in aug.sequence:
            x = augment_grid(x, aug)
        return x
    else:
        raise ValueError(f"Unknown augment {aug}")


def augment_sample(sample: Sample, aug: Augment) -> Sample:
    return Sample(augment_grid(sample.x, aug), augment_grid(sample.y, aug))


def augment_puzzle(puzzle: Puzzle, aug: Augment) -> Puzzle:
    return Puzzle(
        puzzle_id=puzzle.puzzle_id, 
        train=[ augment_sample(sample, aug) for sample in puzzle.train ],
        test=[ augment_sample(sample, aug) for sample in puzzle.test ],
    )
