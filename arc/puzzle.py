from typing import *
from dataclasses import dataclass, field
from abc import ABC, abstractmethod
from numpy.typing import *
from pathlib import Path
import json
import numpy as np
import torch
import matplotlib.pyplot as plt
from matplotlib import colors
from pprint import pprint
import itertools


N_C = 10
C_CHOICES = np.arange(N_C, dtype=np.int8)
ONE_HOTS = np.eye(N_C, dtype=np.float32)

BLACK_VAL = 0
BLUE_VAL = 1
RED_VAL = 2
GREEN_VAL = 3
YELLOW_VAL = 4
GRAY_VAL = 5
PINK_VAL = 6
ORANGE_VAL = 7
LIGHT_BLUE_VAL = 8
DARK_RED_VAL = 9
CMAP = colors.ListedColormap(["#000000", "#1E93FF", "#F93C31", "#4FCC30", "#FFDC00", "#999999", "#E53AA3", "#FF851B", "#87D8F1", "#921231"])
CMAP_NORM = colors.BoundaryNorm(range(11), CMAP.N)


Grid = NDArray[np.int8] # (H, W)


def const_grid(shape, c) -> Grid:
    return np.ones(shape, dtype=np.int8) * c


def grid_from_json(data: List[List[int]]) -> Grid:
    return np.array(data, dtype=np.int8)


def grid_equals(x_1: Grid, x_2: Grid) -> bool:
    return np.array_equal(x_1, x_2)


@dataclass
class Sample:
    x: Grid
    y: Grid

    @classmethod
    def from_json(cls, data: dict) -> 'Sample':
        return cls(x=grid_from_json(data["input"]), y=grid_from_json(data["output"]))

    def __equals__(self, rhs):
        return np.array_equal(self.x, rhs.x) and np.array_equal(self.y, rhs.y)


@dataclass
class Puzzle:
    puzzle_id: str
    train: List[Sample] = field(default_factory=list)
    test: List[Sample] = field(default_factory=list)

    @classmethod
    def from_json(cls, puzzle_id: str, data: dict) -> 'Puzzle':
        return Puzzle(
            puzzle_id=puzzle_id,
            train=[Sample.from_json(sample_data) for sample_data in data["train"]],
            test=[Sample.from_json(sample_data) for sample_data in data["test"]]
        )


def load_puzzles(challenges_path: Path, solutions_path: Path) -> Dict[str, Puzzle]:
    challenges_path = Path(challenges_path)
    solutions_path = Path(solutions_path)
    with open(challenges_path, 'r') as f:
        challenges_json = json.load(f)
    with open(solutions_path, 'r') as f:
        solutions_json = json.load(f)
    puzzles = {}
    for puzzle_id, challenge_json in challenges_json.items():
        solution_json = solutions_json[puzzle_id]
        for i in range(len(challenge_json["test"])):
            challenge_json["test"][i]["output"] = solution_json[i]
        puzzles[puzzle_id] = Puzzle.from_json(puzzle_id, challenge_json)
    return puzzles


class Generator(ABC):

    @abstractmethod
    def gen(self, param: Any) -> Sample:
        """ Given parameters, generate an input-output pair. """
        ...

    @abstractmethod
    def par(self, x: Grid) -> Any:
        """ Given an input example, recover parameters. """
        ...


@dataclass
class GeneratorValidationResult:
    puzzle_id: str
    train: List[Tuple[bool, bool]] = field(default_factory=list)
    test: List[Tuple[bool, bool]] = field(default_factory=list)

    @property
    def train_ok(self) -> bool:
        return all(in_ and out_ for in_, out_ in self.train)

    @property
    def test_ok(self) -> bool:
        return all(in_ and out_ for in_, out_ in self.test)

    @property
    def ok(self) -> bool:
        return self.train_ok and self.test_ok


generators: Dict[str, Generator] = {}

def generator(puzzle_id):
    def dec(generator_cls):
        generators[puzzle_id] = generator_cls()
    return dec


def validate_generator(generator: Generator, puzzle: Puzzle) -> GeneratorValidationResult:
    result = GeneratorValidationResult(puzzle_id=puzzle.puzzle_id)
    for inset, resset in (puzzle.train, result.train), (puzzle.test, result.test):
        for sample in inset: 
            param = generator.par(sample.x)
            gen_sample = generator.gen(param)
            resset.append((grid_equals(gen_sample.x, sample.x), grid_equals(gen_sample.y, sample.y)))
    return result


def regenerate_puzzle(generator: Generator, puzzle: Puzzle) -> Puzzle:
    generated = Puzzle(puzzle.puzzle_id)
    for inset, outset in (puzzle.train, generated.train), (puzzle.test, generated.test):
        for sample in inset: 
            param = generator.par(sample.x)
            gen_sample = generator.gen(param)
            outset.append(gen_sample)
    return generated



def _imshow_grid(x: Grid, ax):
    ax.imshow(x, cmap=CMAP, norm=CMAP_NORM, interpolation="none", aspect="equal", extent=[0, x.shape[1], x.shape[0], 0], origin="upper")
    ax.grid(True, alpha=0.4)
    plt.xticks(np.arange(0, x.shape[1]+1))
    plt.yticks(np.arange(0, x.shape[0]+1))


def plot_puzzle(puzzle: Puzzle, title=None):
    if not title:
        title = puzzle.puzzle_id
    NR = len(puzzle.train) + len(puzzle.test)
    NC = 2
    fig = plt.figure()
    fig.suptitle(title)
    for i, sample in enumerate(puzzle.train + puzzle.test):
        test_or_train = "TRAIN" if i < len(puzzle.train) else "TEST"
        ax = plt.subplot2grid((NR, NC), (i, 0))
        plt.title(f"{title}: {test_or_train} {i+1} input")
        _imshow_grid(sample.x, ax)
        ax = plt.subplot2grid((NR, NC), (i, 1))
        plt.title(f"{title}: {test_or_train} {i+1} output")
        _imshow_grid(sample.y, ax)
    return fig


@generator("007bbfb7")
class Easy1Generator(Generator):

    def gen(self, param):
        assert(isinstance(param, np.ndarray))
        assert(param.shape == (3, 3))
        assert(param.dtype == np.int8)
        x = param.copy()
        y = const_grid((9, 9), BLACK_VAL)
        for (ix, iy), v in np.ndenumerate(param):
            if v != 0:
                y[3*ix:3*ix+3, 3*iy:3*iy+3] = param
        return Sample(x, y)

    def par(self, x: Grid):
        return x


def flood_fill():
    pass


@generator("00d62c1b")
class Easy2Generator(Generator):

    def gen(self, param):
        assert(isinstance(param, np.ndarray))
        assert(param.dtype == np.int8)
        x = param.copy()
        y = const_grid(x.shape, BLACK_VAL)
        # TODO: Find all connected components of 0, not trivial.
        return Sample(x, y)

    def par(self, x: Grid):
        return x


def map_color(cmap: Dict[int,int], x):
    out = x.copy()
    for c_1, c_2 in cmap.items():
        out[x == c_1] = c_2
    return out


def shift_and_check_overlay(x, k) -> bool:
    ny, _ = x.shape
    return ny-k > 0 and (x[k:,:] == x[:ny-k,:]).all()


def yrepeat_kernel(kernel, out_shape):
    ky, kx = kernel.shape
    oy, ox = out_shape
    assert(kx == ox)
    assert(ky < oy)
    n_stacks = -(oy // -ky)
    return np.tile(kernel.T, n_stacks).T[:oy,:]


@generator("017c7c7b")
class Easy3Generator(Generator):

    def gen(self, param):
        assert(isinstance(param, np.ndarray))
        assert(param.dtype == np.int8)
        kernel = param.copy()
        ky, kx = kernel.shape
        assert(ky <= 6)
        assert(kx == 3)
        x = yrepeat_kernel(kernel, (6, kx))
        y = yrepeat_kernel(kernel, (9, kx))
        y = map_color({ 1: 2 }, y)
        sample = Sample(x, y)
        return sample

    def par(self, x: Grid):
        # Determine the smallest y-repeating structure in the input.
        ny, _ = x.shape
        k = 0
        for k in range(1, ny):
            if k >= 3 and shift_and_check_overlay(x, k):
                break
        return x[:k,:]


# TODO: Re-do this with more meaningful primitives: build line, build diagonal, etc...
def build_rhombus(x, start_x, start_y, w, h, c):
    x.T[start_x:start_x+w, start_y] = c
    for i, ry in enumerate(range(start_y + 1, start_y + h)):
        offset_x = start_x + i
        x.T[offset_x, ry] = c
        x.T[offset_x + w, ry] = c
    x.T[start_x+h-1:start_x+h+w-1, start_y+h] = c


def build_shifted_rhombus(x, start_x, start_y, w, h, c):
    x.T[start_x+1:start_x+1+w, start_y] = c
    for i, ry in enumerate(range(start_y + 1, start_y + h)):
        offset_x = start_x + i + 1
        x.T[offset_x, ry] = c
        last = ry == start_y + h - 1
        last_offset_x = offset_x - (1 if last else 0)
        x.T[last_offset_x + w, ry] = c
    x.T[start_x+h-1:start_x+h+w-1, start_y+h] = c


def bounding_box(x, c):
    idxs = np.argwhere(x == c)
    xmin = np.min(idxs[:,1])
    xmax = np.max(idxs[:,1])
    ymin = np.min(idxs[:,0])
    ymax = np.max(idxs[:,0])
    return np.array([[ymin, xmin], [ymax+1, xmax+1]])


def bounding_size(box):
    (ymin, xmin), (ymax, xmax) = box
    return ymax - ymin, xmax - xmin


def bounding_box_region(bbox):
    return slice(bbox[0, 0], bbox[1, 0]), slice(bbox[0, 1], bbox[1, 1])


@generator("025d127b")
class Easy4Generator(Generator):

    def gen(self, param):
        (nx, ny), rhombi = param
        x = const_grid((ny, nx), BLACK_VAL)
        y = const_grid((ny, nx), BLACK_VAL)
        for (rx, ry), w, h, c in rhombi:
            build_rhombus(x, rx, ry, w, h, c)
            build_shifted_rhombus(y, rx, ry, w, h, c)
        return Sample(x, y)

    def par(self, x: Grid):
        (ny, nx) = x.shape
        rhombi = []
        for c in np.unique(x):
            if c == BLACK_VAL:
                continue
            (start_y, start_x), (end_y, end_x) = bounding_box(x, c)
            total_w = end_x - start_x
            total_h = end_y - start_y
            w = total_w - total_h + 2
            h = total_h - 1
            rhombi.append(((start_x, start_y), w, h, c))
        return (nx, ny), rhombi


def rotate(x, k=1):
    return np.rot90(x, k)


def blit(dest, src, loc):
    th,tw=dest.shape
    sh,sw=src.shape
    sr = 0 if -loc[0]<0 else -loc[0]
    fr = sh if loc[0]+sh<=th else sh-(loc[0]+sh-th)
    sc = 0 if -loc[1]<0 else -loc[1]
    fc = sw if loc[1]+sw<=tw else sw-(loc[1]+sw-th)
    loc[0] = max(0,loc[0])
    loc[1] = max(0,loc[1])
    if sr >= fr or sc >= fc:
        return
    dest[loc[0]:loc[0]+sh-sr,loc[1]:loc[1]+sw-sc] = src[sr:fr,sc:fc]


@generator("045e512c")
class Easy5Generator(Generator):

    def gen(self, param):
        (kernel, (ky, kx), kernel_c), children = param
        x = const_grid((21, 21), BLACK_VAL)
        for dir, c in children:
            self._set_incomplete_child_square(x, kernel, (ky, kx), dir, c)
        x[ky:ky+3, kx:kx+3] = kernel
        y = const_grid((21, 21), BLACK_VAL)
        y[ky:ky+3, kx:kx+3] = kernel
        for dir, c in children:
            self._set_complete_child_squares(y, kernel, (ky, kx), kernel_c, dir, c)
        return Sample(x, y)

    def par(self, x: Grid):
        # 1. Find the biggest 2D array by color. We look for exactly a 3x3 box.
        main_c = None
        bbox = np.array([[]])
        for c in np.unique(x):
            if c == BLACK_VAL:
                continue
            bbox = bounding_box(x, c)
            if bounding_size(bbox) == (3, 3):
                main_c = c
                break
        # 2. Save this kernel.
        (ymin, xmin), (ymax, xmax) = bbox
        kernel = x[ymin:ymax, xmin:xmax]
        assert(kernel.shape == (3,3))
        # 3. We additionally need to store the "directions", along with their colors.
        dirs = set()
        children = []
        for c in np.unique(x):
            if c == BLACK_VAL or c == main_c:
                continue
            # 3.1 Select a representative sample from each.
            for coord in np.argwhere(x == c):
                dir = self._child_box_coord_to_direction(bbox, coord)
                if dir in dirs:
                    continue
                children.append((dir, c))
                dirs.add(dir)
        return (kernel, bbox[0,:], main_c), children

    def _direction_to_child_origin(self, kernel_origin, dir, k=1):
        ky, kx = kernel_origin
        diry, dirx = dir
        return (ky + diry * 4 * k, kx + dirx * 4 * k)

    def _set_complete_child_squares(self, x, kernel, kernel_origin, kernel_c, dir, c):
        box = map_color({ kernel_c: c }, kernel)
        for i in range(1,4):
            child_y, child_x = self._direction_to_child_origin(kernel_origin, dir, i)
            blit(x, box, [child_y, child_x])

    def _set_incomplete_child_square(self, x, kernel, kernel_origin, dir, c):
        dir_y, dir_x = dir
        box = const_grid((3,3), BLACK_VAL)
        axis_aligned = dir_y == 0 or dir_x == 0
        if axis_aligned:
            box[:,0] = 1
        else:
            box[[0,0,1], [0,1,0]] = 1
        box = np.logical_and(rotate(box, self._dir_to_rot(dir)), kernel)
        child_y, child_x = self._direction_to_child_origin(kernel_origin, dir)
        x[child_y:child_y+3, child_x:child_x+3] = c * box

    def _dir_to_rot(self, dir):
        if dir == (0,1) or dir == (1,1):
            return 0
        elif dir == (1,0) or dir == (1,-1):
            return 3
        elif dir == (0,-1) or dir == (-1,-1):
            return 2
        elif dir == (-1,0) or dir == (-1,1):
            return 1
        else:
            assert(False)

    def _child_box_coord_to_direction(self, main_bbox, coord):
        """ (y_sign, x_sign) """
        y, x = coord
        (ymin, xmin), (ymax, xmax) = main_bbox
        if y >= ymin and y <= ymax:
            return (0, -1) if x < xmin else (0, 1)
        elif x >= xmin and x <= xmax:
            return (-1, 0) if y < ymin else (1, 0)
        elif x > xmax:
            return (-1, 1) if y < ymin else (1, 1)
        elif x < xmax:
            return (-1, -1) if y < ymin else (1, -1)


@generator("0520fde7")
class Easy6Generator(Generator):

    def gen(self, param):
        lkernel, rkernel = param
        x = const_grid((3, 7), BLACK_VAL)
        x[:,3] = GRAY_VAL
        x[0:3, 0:3] = lkernel
        x[0:3, 4:7] = rkernel
        y = const_grid((3, 3), BLACK_VAL)
        y[:,:] = np.logical_and(lkernel, rkernel)
        y[:,:] = map_color({BLUE_VAL: RED_VAL}, y[:,:])
        return Sample(x, y)

    def par(self, x: Grid):
        assert(x.shape == (3, 7))
        lkernel = x[0:3, 0:3]
        rkernel = x[0:3, 4:7]
        return lkernel, rkernel


def draw_diag(x, i, c):
    N = x.shape[0]
    if i < N:
        x[np.arange(i, -1, -1), np.arange(0, i+1, 1)] = c
    else:
        x[np.arange(N-1, i-N, -1), np.arange(i-N+1, N, 1)] = c


@generator("05269061")
class Easy7Generator(Generator):

    N = 7

    def gen(self, param):
        diag_colors, diag_indices = param
        N = 7
        x = const_grid((N, N), BLACK_VAL)
        for c, diag_index in diag_indices.items():
            draw_diag(x, diag_index, c)
        y = const_grid((N, N), BLACK_VAL)
        ic = 0
        for i in range(0, 2*N-1):
            draw_diag(y, i, diag_colors[ic])
            ic = (ic + 1) % 3
        return Sample(x, y)

    def par(self, x: Grid):
        colors = np.zeros(3, dtype=np.int8)
        diag_indices = {}
        for c in np.unique(x):
            if c == BLACK_VAL:
                continue
            coord = np.argwhere(x == c)[0]
            diag_index = self._diag_index(self.N, coord)
            colors[diag_index % 3] = c
            diag_indices[c] = diag_index
        assert np.all(colors != 0)
        return colors, diag_indices

    def _diag_index(self, N, coord):
        """ The L1 distance away from the origin. """
        return coord[0] + coord[1]


def box_copy(x: Grid, bbox) -> Grid:
     return x[bbox[0, 0]:bbox[1, 0], bbox[0, 1]:bbox[1, 1]]


def box_cut(x: Grid, bbox, replace=BLACK_VAL) -> Grid:
    region = slice(bbox[0, 0], bbox[1, 0]), slice(bbox[0, 1], bbox[1, 1])
    z = x[region]
    x[region] = replace
    return z


def box_paste(x: Grid, z: Grid, coord):
    blit(x, z, coord)


def box_set(x: Grid, bbox, c: np.int8):
    x[bounding_box_region(bbox)] = c


@generator("05f2a901")
class Easy8Generator(Generator):

    def gen(self, param):
        # The red object will be along one of the four axes relative to the box.
        (ny, nx), (blue_y, blue_x), red_bbox, red_shape = param
        x = const_grid((ny, nx), BLACK_VAL)
        x[blue_y:blue_y+2, blue_x:blue_x+2] = LIGHT_BLUE_VAL
        box_paste(x, red_shape, red_bbox[0])
        xrow_1 = x[blue_y,:]
        xrow_2 = x[blue_y+1,:]
        yrow_1 = x[:,blue_x]
        yrow_2 = x[:,blue_x+1]

        y = const_grid((ny, nx), BLACK_VAL)
        y[blue_y:blue_y+2, blue_x:blue_x+2] = LIGHT_BLUE_VAL
        box_paste(y, red_shape, red_bbox[0])
        return Sample(x, y)

    def par(self, x: Grid):
        blue_y, blue_x = np.argwhere(x == LIGHT_BLUE_VAL)[0]
        red_bbox = bounding_box(x, RED_VAL)
        red_shape = box_copy(x, red_bbox)
        return x.shape, (blue_y, blue_x), red_bbox, red_shape

