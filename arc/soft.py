"""
A DSL that operates on "soft grids". Many intuitive operations can be implemented in a differentiable way!
"""

from typing import *
from dataclasses import dataclass, field
from arc.puzzle import *
import torch
import torch.nn.functional as nn


# Actually a "FloatTensor"... but the types comaplain.
SoftGrid = torch.Tensor # (H, W, C)

C = torch.Tensor # (C)
M = torch.Tensor # (H, W)
X = torch.Tensor # (H, W, C)
torch.float


def grid_to_soft(x: Grid) -> SoftGrid:
    return torch.tensor(ONE_HOTS[x], dtype=torch.float32, device="cuda")


def soft_to_grid(x: SoftGrid) -> Grid:
    out = const_grid(x.shape[:2], BLACK_VAL)
    for i, j in itertools.product(range(x.shape[0]), range(x.shape[1])):
        out[i,j] = np.random.choice(C_CHOICES, p=np.array(x[i,j]))
    return out


@dataclass
class SoftSample:
    x: SoftGrid
    y: SoftGrid

    def to_hard(self):
        return Sample(soft_to_grid(self.x), soft_to_grid(self.y))

    @classmethod
    def from_hard(cls, hard: Sample):
        return cls(grid_to_soft(hard.x), grid_to_soft(hard.y))


def const_blend(mask: M, c: C, x: X) -> X:
    return mask * c + (1 - mask) * x


def trans_copy(mask: M, offset: M, x: X) -> X:
    """ Add a translated copy of a masked region. """
    mask_rs = mask.reshape((1,1) + mask.shape)
    offset_rs = offset.reshape((1,1) + offset.shape)
    pad = (offset.shape[0]//2, offset.shape[1]//2)
    translated_mask = nn.conv_transpose2d(mask_rs, offset_rs, padding=pad)[0][0]
    x_rs = x.reshape((1,) + x.shape).permute((3,0,1,2))
    translated_x = nn.conv_transpose2d(x_rs, offset_rs, padding=pad).permute((1,2,3,0))[0]
    return ((1 - translated_mask) * x.permute((2,0,1))  + translated_mask * translated_x.permute((2,0,1))).permute((1,2,0))


def _build_pivot_map(dim):
    M, N = dim
    rx = np.arange(N).reshape(1,N)
    ry = np.arange(M).reshape(1,M)
    pivot_x = np.repeat(rx, M, axis=0)
    pivot_y = np.repeat(ry.T, N, axis=1)
    return np.stack((pivot_x, pivot_y))


def rot_copy(mask: M, pivot: M, x: X) -> X:
    x_masked = mask * x
    x_masked_rs = x_masked.reshape((1,1) + x_masked.shape)
    pivot_rs = pivot.reshape((1,1) + pivot.shape)
    translated_mask = nn.conv_transpose2d(x_masked_rs, pivot_rs)[0][0]
    x_rs = x.reshape((1,) + x.shape).permute((3,0,1,2))
    nn.conv2d()



# def trans(mask: M, offs: M, c: C, alpha: float, x: X):
#     offs = nn.softmax(offs)
#     c = nn.softmax(c)
#     mask[0,0] = 0
